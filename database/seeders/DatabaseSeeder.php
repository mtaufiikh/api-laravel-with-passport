<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;

use Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Muhammad Taufik Hidayat',
            'email' => 'mtaufiikh@gmail.com',
            'password' => Hash::make('rahasia')
        ]);
    }
}
